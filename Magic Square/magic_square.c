#include "magic_square.h"
#include <inttypes.h>


return_value_t is_magic_square(int32_t * square, uint32_t edge_size)
{
	int64_t sum = 0;

	return_value_t are_diagonals_sum_same = compare_diagonals_sum(square, edge_size, &sum);
	if (SUCCESS_FALSE == are_diagonals_sum_same)
	{
		return SUCCESS_FALSE;
	}

	return_value_t are_rows_and_cols_sum_same = compare_rows_and_cols_to_sum(square, edge_size, sum);
	if (SUCCESS_FALSE == are_rows_and_cols_sum_same)
	{
		return SUCCESS_FALSE;
	}

	return SUCCESS_TRUE;
}


return_value_t compare_diagonals_sum(int32_t * square, int32_t edge_size, int64_t * sum)
{
	int64_t first_diagonal_sum = 0;
	int64_t second_diagonal_sum = 0;

	for (uint32_t i = 0; i < edge_size; i++)
	{
		first_diagonal_sum += square[(edge_size * i) + i];
		second_diagonal_sum += square[(edge_size * i) + edge_size - i - 1];
	}

	if (first_diagonal_sum != second_diagonal_sum)
	{
		return SUCCESS_FALSE;
	}

	else
	{
		*sum = first_diagonal_sum;
		return SUCCESS_TRUE;
	}
}


return_value_t compare_rows_and_cols_to_sum(int32_t * square, int32_t edge_size, int64_t sum)
{
	for (uint32_t i = 0; i < edge_size; i++)
	{
		int64_t current_row_sum = sum_row(square + (edge_size * i), edge_size);
		if (sum != current_row_sum)
		{
			return SUCCESS_FALSE;
		}

		int64_t current_col_sum = sum_col(square, edge_size, i);
		if (sum != current_col_sum)
		{
			return SUCCESS_FALSE;
		}
	}

	return SUCCESS_TRUE;
}


int64_t sum_row(int32_t * row, uint32_t row_size)
{
	uint64_t sum = 0;

	for (uint32_t i = 0; i < row_size; i++)
	{
		sum += row[i];
	}

	return sum;
}


int64_t sum_col(int32_t * square, uint32_t edge_size, uint32_t col_index)
{
	uint64_t sum = 0;

	for (uint32_t i = 0; i < edge_size; i++)
	{
		sum += square[(edge_size * i) + col_index];
	}

	return sum;
}