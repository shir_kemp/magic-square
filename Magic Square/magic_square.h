#pragma once
#include <inttypes.h>

typedef enum return_value_e
{
	SUCCESS_TRUE,
	SUCCESS_FALSE,
} return_value_t;


return_value_t is_magic_square(int32_t * square_as_array, uint32_t edge_size);

int64_t sum_row(int32_t* row, uint32_t row_size);

int64_t sum_col(int32_t* square, uint32_t edge_size, uint32_t col_index);

return_value_t compare_diagonals_sum(int32_t* square, int32_t edge_size, int64_t* sum);

return_value_t compare_rows_and_cols_to_sum(int32_t* square, int32_t edge_size, int64_t sum);