#include "magic_square.h"
#include <inttypes.h>
#include <stdio.h>


#define EDGE_SIZE (3)

int main(void)
{
	int32_t true_magic_square[][EDGE_SIZE] = { {4, 3, 8}, {9, 5, 1}, {2, 7, 6} };

	int32_t false_magic_square[][EDGE_SIZE] = { {0, 3, 8}, {9, 5, 1}, {2, 7, 6} };

	return_value_t should_be_true = is_magic_square(&true_magic_square, EDGE_SIZE);
	return_value_t should_be_false = is_magic_square(false_magic_square, EDGE_SIZE);

	if (should_be_true == SUCCESS_TRUE)
	{
		printf("is_magic_square(true_magic_square, %d) returned TRUE\n", EDGE_SIZE);
	}
	else
	{
		printf("is_magic_square(true_magic_square, %d) returned FALSE\n", EDGE_SIZE);
	}

	if (should_be_false == SUCCESS_FALSE)
	{
		printf("is_magic_square(false_magic_square, %d) returned FALSE", EDGE_SIZE);
	}
	else
	{
		printf("is_magic_square(false_magic_square, %d) returned TRUE", EDGE_SIZE);
	}

	return 0;
}